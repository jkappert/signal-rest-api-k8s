FROM registry.access.redhat.com/ubi8/ubi-minimal:latest AS buildcontainer

ARG SIGNAL_CLI_VERSION=0.11.7
ARG LIBSIGNAL_CLIENT_VERSION=0.22.0
ARG SIGNAL_CLI_NATIVE_PACKAGE_VERSION=0.11.7-3

ARG SWAG_VERSION=1.6.7
ARG MAVEN_VERSION=3.9.1
ARG GRAAL_VM_JAVA_VERSION=17
ARG GRAAL_VM_VERSION=22.3.1

ARG BUILD_VERSION_ARG=x86_64

ARG MAVEN_BASE_URL=https://archive.apache.org/dist/maven/maven-3/${MAVEN_VERSION}/binaries
ARG GRAAL_VM_BASE_URL=https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-${GRAAL_VM_VERSION}

ARG PKGS="tar zip gzip tar findutils wget curl unzip gcc glibc-devel zlib-devel java-17-openjdk git zip unzip zlib-devel make golang which hostname"

ARG SIGNAL_CLI_VERSION
ARG LIBSIGNAL_CLIENT_VERSION
ARG SWAG_VERSION
ARG GRAAL_VM_JAVA_VERSION
ARG GRAAL_VM_VERSION
ARG BUILD_VERSION_ARG
ARG SIGNAL_CLI_NATIVE_PACKAGE_VERSION

USER root

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \ 
    && microdnf -y install --nodocs $PKGS \
    && microdnf -y update \
    && microdnf clean all \
    && curl -fsSL -o /tmp/apache-maven.tar.gz ${MAVEN_BASE_URL}/apache-maven-$MAVEN_VERSION-bin.tar.gz \
    && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
    && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn \
    && mkdir -p /opt/graalvm  \
    && curl -fsSL -o /tmp/graalvm-ce-amd64.tar.gz ${GRAAL_VM_BASE_URL}/graalvm-ce-java${GRAAL_VM_JAVA_VERSION}-linux-amd64-${GRAAL_VM_VERSION}.tar.gz \
    && tar -xzf /tmp/graalvm-ce-amd64.tar.gz -C /opt/graalvm --strip-components=1  \
    && /opt/graalvm/bin/gu --auto-yes install native-image \
    && rm -f /tmp/apache-maven.tar.gz \
    && mkdir -p /project

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"
ENV GRAALVM_HOME /opt/graalvm
ENV JAVA_HOME /opt/graalvm
ENV WORK_DIR /project
ENV PATH $PATH:$JAVA_HOME/bin
ENV GOPATH=/project/go

COPY ext/libraries/libsignal-client/v${LIBSIGNAL_CLIENT_VERSION} /tmp/libsignal-client-libraries

RUN cp /tmp/libsignal-client-libraries/x86-64/libsignal_jni.so /tmp/libsignal_jni.so 

ENV JAVA_OPTS="-Djdk.lang.Process.launchMechanism=vfork"

ENV LANG en_US.UTF-8

RUN cd /tmp/ \
	&& git clone https://github.com/swaggo/swag.git swag-${SWAG_VERSION} \	
	&& cd swag-${SWAG_VERSION} \
	&& git checkout -q v${SWAG_VERSION} \
	&& make -s < /dev/null > /dev/null \
	&& cp /tmp/swag-${SWAG_VERSION}/swag /usr/bin/swag \
	&& rm -r /tmp/swag-${SWAG_VERSION}

RUN cd /tmp/ \
	&& wget -nv https://github.com/AsamK/signal-cli/releases/download/v${SIGNAL_CLI_VERSION}/signal-cli-${SIGNAL_CLI_VERSION}-Linux.tar.gz -O /tmp/signal-cli.tar.gz \
	&& tar xf signal-cli.tar.gz

#RUN cd /tmp \
#		&& git clone https://github.com/AsamK/signal-cli.git signal-cli-${SIGNAL_CLI_VERSION}-source \
#		&& cd signal-cli-${SIGNAL_CLI_VERSION}-source \
#		&& git checkout -q v${SIGNAL_CLI_VERSION} \
#		&& cd /tmp && tar xf /tmp/graalvm-ce-amd64.tar.gz \
#		&& cd /tmp/signal-cli-${SIGNAL_CLI_VERSION}-source \
#		&& sed -i 's/Signal-Android\/5.22.3/Signal-Android\/5.51.7/g' src/main/java/org/asamk/signal/BaseConfig.java \ 
#		&& gu install native-image \
#		&& ./gradlew -q nativeCompile \
#        && rm -rf /tmp/graalvm-ce-amd64.tar.gz

RUN ls /tmp/signal-cli-${SIGNAL_CLI_VERSION}/lib/libsignal-client-${LIBSIGNAL_CLIENT_VERSION}.jar || (echo "\n\nsignal-client jar file with version ${LIBSIGNAL_CLIENT_VERSION} not found. Maybe the version needs to be bumped in the signal-cli-rest-api Dockerfile?\n\n" && echo "Available version: \n" && ls /tmp/signal-cli-${SIGNAL_CLI_VERSION}/lib/libsignal-client-* && echo "\n\n" && exit 1)

# workaround until upstream is fixed
RUN cd /tmp/signal-cli-${SIGNAL_CLI_VERSION}/lib \
	&& unzip signal-cli-${SIGNAL_CLI_VERSION}.jar \
	&& sed -i 's/Signal-Android\/5.22.3/Signal-Android\/5.51.7/g' org/asamk/signal/BaseConfig.class \
	&& zip -r signal-cli-${SIGNAL_CLI_VERSION}.jar org/ META-INF/ \
	&& rm -rf META-INF \
	&& rm -rf org

RUN cd /tmp/ \
	&& zip -qu /tmp/signal-cli-${SIGNAL_CLI_VERSION}/lib/libsignal-client-${LIBSIGNAL_CLIENT_VERSION}.jar libsignal_jni.so \
	&& zip -qr signal-cli-${SIGNAL_CLI_VERSION}.zip signal-cli-${SIGNAL_CLI_VERSION}/* \
    && unzip -q /tmp/signal-cli-${SIGNAL_CLI_VERSION}.zip -d /opt \
    && unzip -q /tmp/signal-cli-${SIGNAL_CLI_VERSION}.zip -d /project \
	&& rm -f /tmp/signal-cli-${SIGNAL_CLI_VERSION}.zip

RUN cd /tmp \
    && git clone https://github.com/dadevel/alertmanager-signal-receiver.git alertmanager-signal-receiver-source \
    && cd alertmanager-signal-receiver-source \
    && go build -o ./alertmanager-signal-receiver ./cmd/main.go \
    && strip ./alertmanager-signal-receiver

RUN cd /tmp \
    && git clone https://gitlab.com/thefinn93/alertmanager-signald.git alertmanager-signald-source \
    && cd alertmanager-signald-source \
    && go build -o ./alertmanager-signald

COPY src/api /project/signal-cli-rest-api-src/api
COPY src/client /project/signal-cli-rest-api-src/client
COPY src/utils /project/signal-cli-rest-api-src/utils
COPY src/scripts /project/signal-cli-rest-api-src/scripts
COPY src/main.go /project/signal-cli-rest-api-src/
COPY src/go.mod /project/signal-cli-rest-api-src/
COPY src/go.sum /project/signal-cli-rest-api-src/


# build signal-cli-rest-api
RUN cd /project/signal-cli-rest-api-src && swag init && go test ./client -v && go build

# build supervisorctl_config_creator
#RUN cd /project/signal-cli-rest-api-src/scripts && go build -o jsonrpc2-helper 

# Start a fresh container for release container

FROM registry.access.redhat.com/ubi8/ubi-minimal:latest

ENV GIN_MODE=release

ARG SIGNAL_CLI_VERSION=0.11.7
ARG LIBSIGNAL_CLIENT_VERSION=0.22.0
ARG SIGNAL_CLI_NATIVE_PACKAGE_VERSION=0.11.7-3

ARG SWAG_VERSION=1.6.7
ARG MAVEN_VERSION=3.9.1
ARG GRAAL_VM_JAVA_VERSION=17
ARG GRAAL_VM_VERSION=22.3.1

ARG BUILD_VERSION_ARG=x86_64

ENV BUILD_VERSION=$BUILD_VERSION_ARG

ARG PKGS="nmap-ncat java-17-openjdk util-linux hostname supervisor"

USER root

RUN rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm \
    && microdnf -y install --nodocs $PKGS \
    && microdnf -y update \
    && microdnf clean all

COPY --from=buildcontainer /project/signal-cli-rest-api-src/signal-cli-rest-api /usr/bin/signal-cli-rest-api
COPY --from=buildcontainer /opt/signal-cli-${SIGNAL_CLI_VERSION} /opt/signal-cli-${SIGNAL_CLI_VERSION}
#COPY --from=buildcontainer /tmp/signal-cli-${SIGNAL_CLI_VERSION}-source/build/native/nativeCompile/signal-cli /opt/signal-cli-${SIGNAL_CLI_VERSION}/bin/signal-cli-native
COPY --from=buildcontainer /tmp/alertmanager-signal-receiver-source/alertmanager-signal-receiver /usr/bin/
#COPY --from=buildcontainer /project/signal-cli-rest-api-src/scripts/jsonrpc2-helper /usr/bin/jsonrpc2-helper
COPY --from=buildcontainer /tmp/alertmanager-signald-source/alertmanager-signald /usr/bin
COPY supervisord.conf /opt/signal-api/supervisor/supervisord.conf

RUN useradd --no-log-init -M -d /opt/signal-api -s /bin/bash -u 1001 -g 0 signal-api \
	&& ln -s /opt/signal-cli-${SIGNAL_CLI_VERSION}/bin/signal-cli /usr/bin/signal-cli \
	&& ln -s /opt/signal-cli-${SIGNAL_CLI_VERSION}/bin/signal-cli-native /usr/bin/signal-cli-native \
	&& mkdir -p /opt/signal-api/signal-cli-config/ \
	&& mkdir -p /opt/signal-api/.local/share/signal-cli

COPY entrypoint.sh /opt/signal-api/entrypoint.sh

ENV SIGNAL_CLI_CONFIG_DIR=/opt/signal-api/data
ENV SIGNAL_CLI_UID=1001
ENV SIGNAL_CLI_GID=0

RUN mkdir /opt/signal-api/data \
    && chown -R 1001:0 /opt/signal-api \
    && chown -R 1001:0 /opt/signal-cli-${SIGNAL_CLI_VERSION} \
    && chmod +x /opt/signal-api/entrypoint.sh \
    && chown 1001:0 /opt/signal-api/entrypoint.sh \
	&& chmod +x /usr/bin/alertmanager-signal-receiver \
    && mkdir -p /opt/signal-api/supervisor/log \
    && touch /opt/signal-api/supervisor/log/supervisord.log \
    && chmod 777 /opt/signal-api/supervisor/log/supervisord.log \
    && chown -R 1001:0 /opt/signal-api/supervisor/log \
    && touch /opt/signal-api/supervisor/supervisor.sock \
    && chown 1001:0 /opt/signal-api/supervisor/supervisor.sock \
    && chown -R 1001:0 /opt/signal-api 

EXPOSE 8080 
EXPOSE 9709
EXPOSE 9001

USER 1001

CMD ["supervisord", "-c", "/opt/signal-api/supervisor/supervisord.conf"]
#ENTRYPOINT ["/opt/signal-api/entrypoint.sh"]