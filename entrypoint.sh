#!/bin/sh

set -x
set -e

[ -z "${SIGNAL_CLI_CONFIG_DIR}" ] && echo "SIGNAL_CLI_CONFIG_DIR environmental variable needs to be set! Aborting!" && exit 1;

export HOST_IP=$(hostname -I | awk '{print $1}')

# Start API as signal-api user
exec signal-cli-rest-api -signal-cli-config=${SIGNAL_CLI_CONFIG_DIR} 
